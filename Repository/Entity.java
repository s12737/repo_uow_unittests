package Repository;
public class Entity {
    private int id;
    private EntityState state;
    
    public int getId() { return this.id; }
    public void setId(int id) { this.id = id; }
    public EntityState getState() { return this.state; }
    public void seState(EntityState state) { this.state = state; }
}
