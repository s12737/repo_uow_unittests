package Rule;

public interface CheckRule<TEntity> {
	CheckResult checkRule(TEntity entity);
}
