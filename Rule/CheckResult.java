package Rule;

public class CheckResult {
	private String message;
	private EnumName result;
	public String getMessage() {
		return message;
	}
	public void setMessage(String message) {
		this.message = message;
	}
	public EnumName getResult() {
		return result;
	}
	public void setResult(EnumName result) {
		this.result = result;
	}
	
	
}
