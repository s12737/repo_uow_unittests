package Rule;

public class NameRule implements CheckRule {

	@Override
	public CheckResult checkRule(Object entity) {
		Rule.CheckResult result = new Rule.CheckResult();

		if(entity.getFirstName() == null) {
			result.setResult(EnumName.Exception);
		}
		else if(entity.getFirstName().Containts("!@#$%^&*()_+?><")) {
			result.setResult(EnumName.Error);
		}
		else if(entity.getFirstName() == "") {
			result.setResult(EnumName.Error);
		}
		else
			result.setResult(EnumName.Ok);

		return result;
}
}
