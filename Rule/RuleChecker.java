package Rule;

import java.util.List;


public class RuleChecker<TEntity> {
	private List<CheckRule<TEntity>> rules;
	public List<CheckResult> check(TEntity entity) {
		return null;
	}
	
	public List<CheckRule<TEntity>> getRules() {
		return rules;
	}

	public void setRules(List<CheckRule<TEntity>> rules) {
		this.rules = rules;
	}
	
}
