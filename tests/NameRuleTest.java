package tests;

import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.*;

/**
 * Created by Paul on 23.11.2015.
 */
public class NameRuleTest {
    @Test
    public void testProperlyName() throws Exception {
        CheckRule rule = new NameRule();
        Person person = new main.Person();
        person.setFirstName("Pafcio");

        EnumName actual = (rule.checkRule(person)).getResult();
        assertEquals(EnumName.Ok, actual);
    }
    @Test
    public void testNameContainsSpecialCharacters() throws Exception {
        CheckRule rule = new NameRule();
        Person person = new main.Person();
        person.setFirstName("Paf!!@#!io");

        EnumName actual = (rule.checkRule(person)).getResult();
        assertEquals(EnumName.Error, actual);
    }
    @Test
    public void testNameIsEmpty() throws Exception {
        CheckRule rule = new NameRule();
        Person person = new main.Person();
        person.setFirstName("");

        EnumName actual = (rule.checkRule(person)).getResult();
        assertEquals(EnumName.Error, actual);
    }
    @Test
    public void testNameIsNull() throws Exception {
        CheckRule rule = new NameRule();
        Person person = new main.Person();

        EnumName actual = (rule.checkRule(person)).getResult();
        assertEquals(EnumName.Exception, actual);
    }
}